/**
 * SPDX-License-Identifier: CC0-1.0
 *
 * Fix benchmarks in phoronix.com articles
 *
 * Copyright (C) 2021 Wojtek Kosior <koszko@koszko.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the CC0 1.0 Universal License as published by
 * the Creative Commons Corporation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * CC0 1.0 Universal License for more details.
 */

/* Use with https://www.phoronix.com/*** */

/*
 * Phoronix normally includes scripts that call document.write() to inject
 * <img> tags. The most obvious way o code a fix would then be do download and
 * parse the contents of those scripts. CORS, however, doesn't allow this.
 * Instead, we notice that the openbenchmarking embed script url is related to
 * the actual image url we need, so we can create <img>'s from it straight away.
 */
for (const script of document.scripts) {
    const match = /openbenchmarking.org\/+(embed.php\?.*)p=0$/.exec(script.src);
    if (!match) continue;

    const img = document.createElement("img");
    img.src = `https://openbenchmarking.org/${match[1]}p=2`;
    img.setAttribute("type", "image/svg+xml");
    img.setAttribute("width", "100%");
    img.setAttribute("height", "auto");

    script.parentElement.insertBefore(img, script);
}
