/**
 * SPDX-License-Identifier: CC0-1.0
 *
 * View OpenCores projects list without nonfree js
 *
 * Copyright (C) 2021 Wojtek Kosior <koszko@koszko.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the CC0 1.0 Universal License as published by
 * the Creative Commons Corporation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * CC0 1.0 Universal License for more details.
 */

/* use with https://opencores.org/projects */

let data = JSON.parse(document.getElementById("__NEXT_DATA__").textContent);
let sections = {};
for (let h1 of document.getElementsByClassName("cMJCrc")) {
    let ul = document.createElement("ul");
    if (h1.nextElementSibling !== null)
	h1.parentNode.insertBefore(ul, h1.nextElementSibling);
    else
	h1.parentNode.appendChild(ul);

    sections[h1.children[1].firstChild.textContent] = ul;
}

for (let prop of data.props.pageProps.list) {
    let ul = sections[prop.category];
    if (ul === undefined) {
	console.log(`unknown category "${prop.category}" for project "${prop.title}"`);
	continue;
    }

    let li = document.createElement("li");
    let a = document.createElement("a");
    a.setAttribute("href", "/projects/" + prop.slug);
    a.textContent = prop.title;

    li.appendChild(a);
    ul.appendChild(li);
}
