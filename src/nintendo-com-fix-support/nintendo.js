/**
 * SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions
 *
 * Make Nintendo Support reasonably consumable when its JS is blocked.
 *
 * Copyright (C) 2022 Wojtek Kosior <koszko@koszko.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute forms of that code without the copy of the GNU
 * GPL normally required by section 4, provided you include this
 * license notice and, in case of non-source distribution, a URL
 * through which recipients can access the Corresponding Source.
 * If you modify file(s) with this exception, you may extend this
 * exception to your version of the file(s), but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * As a special exception to the GPL, any HTML file which merely
 * makes function calls to this code, and for that purpose
 * includes it by reference shall be deemed a separate work for
 * copyright law purposes. If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 * I, Wojtek Kosior, thereby promise not to sue for violation of this file's
 * license. Although I request that you do not make use of this code in a
 * proprietary program, I am not going to enforce this in court.
 */

/* Use with: https://*.nintendo.com/app/*** */

/* Show the main text. */
document.documentElement.classList.remove("no-js");

/* Fix menu bar (TODO: also fix the hamburger version for smaller screens). */
const title_bar = document.querySelector(".title-bar");
if (title_bar !== null)
    title_bar.style.display = "none";

const top_menu = document.querySelector(".top-bar-left>.menu");
if (top_menu !== null) {
    top_menu.classList.add("dropdown");
    top_menu.classList.remove("accordion-menu");
}

function show_submenu(submenu) {
    submenu.classList.add("js-dropdown-active");
}

function hide_submenu(submenu) {
    submenu.classList.remove("js-dropdown-active");
}

for (const submenu_parent of
     document.getElementsByClassName("rn_ProductCategoryItem")) {
    submenu_parent.classList.add("is-dropdown-submenu-parent");
    submenu_parent.classList.add("opens-right");

    const submenu = submenu_parent.querySelector(".rn_SubItemList");
    if (submenu === null)
	continue;

    submenu.classList.add("submenu");
    submenu.classList.add("is-dropdown-submenu");
    submenu.classList.add("first-sub");

    submenu_parent.addEventListener("mouseenter", () => show_submenu(submenu));
    submenu_parent.addEventListener("mouseleave", () => hide_submenu(submenu));
}

/* Fix search. */
const search_input = document.getElementById("rn_KeywordText_5_Text");

function on_search_submit(event) {
    event.preventDefault();

    if (!search_input.value)
	return;

    const encoded = encodeURIComponent(search_input.value);
    window.location.pathname = `/app/answers/list/kw/${encoded}/search/1`;
}

if (search_input !== null && search_input.form !== null)
    search_input.form.addEventListener("submit", on_search_submit);

/* Fix navigation to other pages of search results. */
for (const results_page_link of
     document.querySelectorAll('[href^="/app/answers/list/page/"]')) {
    const page_nr = /[0-9]*$/.exec(results_page_link.href);
    const current_path = window.location.pathname;
    results_page_link.href = current_path
	.replace("list/kw", "list/st/5/kw")
	.replace(/(search|page)\/[0-9]*$/, `page/${page_nr}`);
}
