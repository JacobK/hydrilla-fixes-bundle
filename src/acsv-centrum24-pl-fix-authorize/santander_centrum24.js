/**
 * SPDX-License-Identifier: CC0-1.0
 *
 * Fix SMS code submission on https://acsv.centrum24.pl/ACS/servlet/ACSAuthoriz
 *
 * Copyright (C) 2021 Wojtek Kosior <koszko@koszko.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the CC0 1.0 Universal License as published by
 * the Creative Commons Corporation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * CC0 1.0 Universal License for more details.
 */

const submit_button = document.getElementById("submit");
submit_button.classList.remove("disabled");
submit_button.removeAttribute("disabled");

console.log(document.querySelectorAll("noscript"));

for (const noscript_element of document.querySelectorAll("noscript"))
    noscript_element.remove();
