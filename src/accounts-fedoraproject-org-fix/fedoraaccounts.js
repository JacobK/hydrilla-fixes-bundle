/**
 * SPDX-License-Identifier: CC0-1.0
 *
 * Fix registration of a Fedora account
 *
 * Copyright (C) 2021 Wojtek Kosior <koszko@koszko.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the CC0 1.0 Universal License as published by
 * the Creative Commons Corporation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * CC0 1.0 Universal License for more details.
 */

/* Haketilo fix to use with https://accounts.fedoraproject.org */

var by_id = id => document.getElementById(id);

var login_register_tabs = ['login', 'register'].map(by_id);
var login_register_buttons = login_register_tabs.map(e => by_id(`${e.id}-tab`))

function switch_tab(i)
{
    login_register_buttons[i].classList.add('active');
    login_register_buttons[1 - i].classList.remove('active');

    login_register_tabs[i].classList.add('show', 'active');
    login_register_tabs[1 - i].classList.remove('show', 'active');
}

for (const i of [0, 1]) {
    login_register_buttons[i].addEventListener('click', () => switch_tab(i));
    login_register_buttons[i].href = '#'
}
