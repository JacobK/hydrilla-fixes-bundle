/**
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright © 2021 jahoti <jahoti@tilde.team>
 * Copyright 2022 Wojtek Kosior <koszko@koszko.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * I, Wojtek Kosior, thereby promise not to sue for violation of this file's
 * license. Although I request that you do not make use of this code in a way
 * incompliant with the license, I am not going to enforce this in court.
 */

/* Make cookie notice closable. */
const to_rem = document.querySelectorAll(".cc-policy-overlay, .cc-policy");

for (const cc_button of document.querySelectorAll(".cc-policy .cc-buttons"))
    cc_button.addEventListener("click", () => to_rem.forEach(n => n.remove()));

/* Make country selection popup closable. */
const langbox_remover = () => document.getElementById("langbox").remove();

const link_selector = ".country_flag a[onclick], .country_button a[onclick]";
for (const a of document.querySelectorAll(link_selector))
    a.addEventListener("click", langbox_remover);
