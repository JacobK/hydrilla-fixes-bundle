/**
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright © 2021 jahoti <jahoti@tilde.team>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const formId = document.querySelector('input[name="page_id"]').value;
const form = document.querySelector('form[name="specbuilder"]');
const noVat = document.getElementById('running_total_ex');
const incVat = document.getElementById('running_total_inc');

function updatePrice() {
	const xhr = new XMLHttpRequest();

	var names = [], values = [];
	for (var inputElement of form.querySelectorAll('select, input[type="radio"]')) {
		if (inputElement.name && (inputElement.checked || inputElement.tagName === 'SELECT')) {
			names.push(inputElement.name);
			values.push(inputElement.value);
		}
	}

	const url = '/ajax/running_total.php?categories=' + names.join('%2C') +
		    '%2C&products=' + values.join('%2C') + '%2C&q=' + form.querySelector('input[name="q"]').value + '&form_id=' + formId;

	xhr.onreadystatechange = priceUpdated;
	xhr.open('GET', url, true);
	xhr.send();
}

function priceUpdated() {
	if (this.readyState === 4) {
		if (this.status === 200) {
			const parts = this.responseText.split("'");
			noVat.innerText = parts[parts.length - 6];
			incVat.innerText = parts[parts.length - 2];
		}
		else alert('Failed to get data: HTTP status code ' + this.status);
	}
}

const button = document.createElement('button');
button.innerText = 'Update Prices';
button.onclick = updatePrice;
document.querySelector('.price-holder.price-finance-holder').append(button);
