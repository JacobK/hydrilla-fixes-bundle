/**
 * SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions
 *
 * Show driver downloads on the TP-Link website.
 *
 * Copyright (C) 2022 Jacob K
 * Copyright (C) 2022 Wojtek Kosior <koszko@koszko.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute forms of that code without the copy of the GNU
 * GPL normally required by section 4, provided you include this
 * license notice and, in case of non-source distribution, a URL
 * through which recipients can access the Corresponding Source.
 * If you modify file(s) with this exception, you may extend this
 * exception to your version of the file(s), but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * As a special exception to the GPL, any HTML file which merely
 * makes function calls to this code, and for that purpose
 * includes it by reference shall be deemed a separate work for
 * copyright law purposes. If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 * I, Wojtek Kosior, thereby promise not to sue for violation of this file's
 * license. Although I request that you do not make use of this code in a
 * proprietary program, I am not going to enforce this in court.
 */

/*
 * Use with:
 * * https://www.tp-link.com/us/support/**
 * * https://www.tp-link.com/au/support/**
 * * etc...
 */

/* show drivers by default (TODO: make it so you click the "Driver" button to
	reveal, and also let people click the "Setup Video" and "FAQ" buttons) */
if (document.getElementById("content_Driver")) {
	document.getElementById("content_Driver").style.display = "initial";
}

/* TODO: Show FAQ sections here (https://www.tp-link.com/us/support/faq/46/),
	using the selector `#article .faq-slide + div` and setting the display to
	not "none" (probably "initial" (actually "inherit" might be better than
	"initial" generally, idk))) */

// show support version selection drop down menus (example: https://www.tp-link.com/us/support/download/tl-wn725n/)
if (document.getElementsByClassName("select-version")[0]) {
	const dropdown = document.getElementsByClassName("select-version")[0].getElementsByTagName("dd")[0].getElementsByTagName("ul")[0];
	document.getElementsByClassName("select-version")[0].getElementsByTagName("dd")[0].addEventListener("click", function() {
		if (dropdown.style.getPropertyValue("display") == "inherit") {
			dropdown.style.setProperty("display", "none");
		} else {
			dropdown.style.setProperty("display", "inherit");
		}
	});
}

/* Enable drop-down lists. */
let first_item = null;

let viewed_item = null;

function change_item_visibility(item, visible) {
    const icon     = item.querySelector("h2 i");
    const item_box = item.querySelector(".item-box");

    if (icon === null || item_box === null)
	return;

    icon.classList[visible ? "add" : "remove"]("tp-active");

    item_box.style.display = visible ? "block" : "none";
}

function toggle_item(item) {
    if (item === viewed_item) {
	change_item_visibility(item, false);

	viewed_item = null;
    } else {
	if (viewed_item !== null)
	    change_item_visibility(viewed_item, false);

	change_item_visibility(item, true);

	viewed_item = item;
    }
}

for (const list_item of document.querySelectorAll("#list .item")) {
    first_item = first_item || list_item;

    const heading = list_item.querySelector("h2");
    if (heading !== null)
	heading.addEventListener("click", () => toggle_item(list_item));
}

if (first_item !== null)
    toggle_item(first_item);
