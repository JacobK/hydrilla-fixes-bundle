/* SPDX-License-Identifier: CC0-1.0
 *
 * Fix automatic redirect on Stack Exchange login page.
 *
 * Copyright (C) 2022 Wojtek Kosior
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the CC0 1.0 Universal License as published by
 * the Creative Commons Corporation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * CC0 1.0 Universal License for more details.
 */

/* Use with https://meta.stackexchange.com/users/login */

const redirect_link = document.querySelector(".manual-redirect a");
if (redirect_link !== null)
    window.location.href = redirect_link.href;
