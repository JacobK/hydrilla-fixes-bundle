/**
 * SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions
 *
 * Show the terms of service on the Tengo Internet login page.
 *
 * Copyright (C) 2022 Jacob K
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute forms of that code without the copy of the GNU
 * GPL normally required by section 4, provided you include this
 * license notice and, in case of non-source distribution, a URL
 * through which recipients can access the Corresponding Source.
 * If you modify file(s) with this exception, you may extend this
 * exception to your version of the file(s), but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * As a special exception to the GPL, any HTML file which merely
 * makes function calls to this code, and for that purpose
 * includes it by reference shall be deemed a separate work for
 * copyright law purposes. If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Use with: https://admin.tengointernet.com/login/users/click-connect */

/*
		All I did to make this fix was look at the HTML, selecting on the
	"Terms & Conditions" link and then looking at it's attributes, as well as
	the HTML elements those attributes pointed to.
		This is a Wi-Fi login portal app, but it is possible to connect to the
	internet without it; you just can't see the terms of service.
*/

document.getElementById("termsModal").style = ""; // remove the "display: none;" style
document.getElementById("termsModal").className = "fade in"; // remove the "modal" class
[...document.getElementsByTagName("a")].forEach(function(element){
	// make every a element link to the target it's supposed to
    element.href = element.getAttribute("data-target");
});
