/**
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright © 2021 jahoti <jahoti@tilde.team>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var pathParts = location.pathname.split('/'), itemRef = pathParts[pathParts.length - 1];

// Generate a function which, when invoked, loads the catalog holdings starting at i (one-indexed) focused on loc
function generateGoTo(i, set_loc) {
	return function (e) {
		e.preventDefault();
		; // If this is a new search, "set_loc" won't be set; set it
		var xhr = new XMLHttpRequest(), loc = set_loc || encodeURIComponent(locInput.value);
		xhr.onreadystatechange = function () {
			if (this.readyState === 4) {
				if (this.status === 200) {
					retrieved.innerHTML = this.responseText;

					var i, node = document.getElementById('libslocator');
					node.parentNode.removeChild(node);
					for (node of retrieved.querySelectorAll('a[href^="javascript:findLibs(\'\', "]')) {
						i = parseInt(node.href.split(',', 2)[1]);
						node.onclick = generateGoTo(i, loc);
					}
				}
				else alert('Search failed: response code ' + this.status);
			}
		}

		xhr.open('GET', 'https://www.worldcat.org/wcpa/servlet/org.oclc.lac.ui.ajax.ServiceServlet?wcoclcnum=' + itemRef + '&start_holding='
				+ i + '&serviceCommand=holdingsdata&loc=' + loc, true);
		xhr.send();
	};
}


var retriever = document.querySelector('.retrieving'), retrieved = document.getElementById('donelocator');

var locForm = document.createElement('form'), locLabel = document.createElement('label'), locInput = document.createElement('input'),
	locSubmit = document.createElement('input');

locForm.appendChild(locLabel);
locForm.appendChild(locInput);
locForm.appendChild(locSubmit);

locInput.name = locLabel.htmlFor = 'cat_location';
locInput.type = 'text';
locInput.required = 'yes';
locLabel.innerText = 'Find copies closest to: ';
locSubmit.value = 'Go';
locSubmit.type = 'submit';
locForm.onsubmit = generateGoTo(1);

retriever.parentNode.replaceChild(locForm, retriever);
