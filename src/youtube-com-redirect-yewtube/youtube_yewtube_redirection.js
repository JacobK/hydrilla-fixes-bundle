/**
 * SPDX-License-Identifier: CC0-1.0
 *
 * Redirect youtube.com to yewtu.be
 *
 * Copyright (C) 2021 Wojtek Kosior <koszko@koszko.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the CC0 1.0 Universal License as published by
 * the Creative Commons Corporation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * CC0 1.0 Universal License for more details.
 * Available under the terms of Creative Commons Zero.
 */

/* Use with https://www.youtube.com/*** */

window.location.href = make_yewtube_url(document.URL);
