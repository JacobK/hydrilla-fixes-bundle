/**
 * SPDX-License-Identifier: CC0-1.0
 *
 * Watch vaticannews.va embedded YouTube videos on yewtu.be instead
 *
 * Copyright (C) 2021 Wojtek Kosior <koszko@koszko.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the CC0 1.0 Universal License as published by
 * the Creative Commons Corporation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * CC0 1.0 Universal License for more details.
 * Available under the terms of Creative Commons Zero.
 */

/* Use with https://www.vaticannews.va/*** */

for (const iframe of document.querySelectorAll("iframe[data-src]")) {
    const youtube_url = iframe.getAttribute("data-src");
    iframe.setAttribute("src", make_yewtube_url(youtube_url));
}
