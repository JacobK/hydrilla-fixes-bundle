/**
 * SPDX-License-Identifier: CC0-1.0
 *
 * Unhide RGS exhibitions page after it loads without nonfree js.
 *
 * Copyright (C) 2021 Wojtek Kosior <koszko@koszko.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the CC0 1.0 Universal License as published by
 * the Creative Commons Corporation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * CC0 1.0 Universal License for more details.
 */

const unhide_style = "visibility: visible !important; opacity: 1 !important;";
document.documentElement.setAttribute("style", unhide_style);
