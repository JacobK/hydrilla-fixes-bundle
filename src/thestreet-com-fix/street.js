/**
 * SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions
 *
 * On The Street, remove broken ads and show some images properly.
 *
 * Copyright (C) 2022 Jacob K
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute forms of that code without the copy of the GNU
 * GPL normally required by section 4, provided you include this
 * license notice and, in case of non-source distribution, a URL
 * through which recipients can access the Corresponding Source.
 * If you modify file(s) with this exception, you may extend this
 * exception to your version of the file(s), but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * As a special exception to the GPL, any HTML file which merely
 * makes function calls to this code, and for that purpose
 * includes it by reference shall be deemed a separate work for
 * copyright law purposes. If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Use with: https://www.thestreet.com/*** */

/* NOTE: The code that fixes the images may reveal some information about your 
	screen size. I'm not sure about the details (but I'm pretty sure any page
	could include the "srcset" attribute and get the same information). */

// remove blank advertisement banners that obscure the screen
[...document.getElementsByClassName("m-balloon-header--ad is-below-header-ad")].forEach(function(element){element.remove();});
[...document.getElementsByClassName("m-fixedbottom-ad--container")].forEach(function(element){element.remove();});

// show images
// assumes all elements with class "is-waiting-to-load" are pictures
[...document.getElementsByClassName("is-waiting-to-load")].forEach(function(picture){ // for each picture waiting to load
	[...picture.children].forEach(function(element){ // for each child of each picture
		if (element.getAttribute("data-srcset")) { // if "data-srcset" exists
			element.srcset = element.getAttribute("data-srcset"); // set "srcset" to "data-srcset"
		}
	});
	picture.className = "is-loaded"; /* This will probably mark some 
		non-properly-loaded pictures as loaded, but changing this class is
		necessary for the properly loaded images to display */
});

/* TODO: handle twitter embeds (convert to iframe, then let LibRedirect or a
	different Haketilo script handle the iframe) */

/*
 * TODO: handle .m3u8 videos (some library will be needed to process this
 * format)
 */
