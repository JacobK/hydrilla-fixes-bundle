/**
 * SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions
 *
 * Make videos on archive.org playable inline without relying on site-served js
 *
 * Copyright (C) 2021 Wojtek Kosior <koszko@koszko.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute forms of that code without the copy of the GNU
 * GPL normally required by section 4, provided you include this
 * license notice and, in case of non-source distribution, a URL
 * through which recipients can access the Corresponding Source.
 * If you modify file(s) with this exception, you may extend this
 * exception to your version of the file(s), but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * As a special exception to the GPL, any HTML file which merely
 * makes function calls to this code, and for that purpose
 * includes it by reference shall be deemed a separate work for
 * copyright law purposes. If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * I, Wojtek Kosior, thereby promise not to sue for violation of this file's
 * license. Although I request that you do not make use of this code in a
 * proprietary program, I am not going to enforce this in court.
 */

const theatre_ia = document.getElementById("theatre-ia");

let srcs = [];

let ogv_src = null;
let webm_src = null;
let mp4_src = null;

function process_link(link)
{
    ogv_src  = ogv_src  || (/\.ogv$/.test(link)  && link);
    webm_src = webm_src || (/\.webm$/.test(link) && link);
    mp4_src  = mp4_src  || (/\.mp4$/.test(link)  && link);
}

if (theatre_ia) {
    for (const a of
	 document.querySelectorAll(".item-download-options a.download-pill"))
	process_link(a.href);

    for (const link of document.querySelectorAll("link[itemprop=contentUrl]"))
	process_link(link.href);

    srcs = [
	{src: ogv_src,  type: "video/ogg"},
	{src: webm_src, type: "video/webm"},
	{src: mp4_src,  type: "video/mp4"}
    ].filter(src => src.src);
}

if (srcs.length > 0) {
    const video = document.createElement("video");

    for (const src of srcs) {
	const source = document.createElement("source");
	Object.assign(source, src);
	video.appendChild(source);
    }

    video.setAttribute("width", "100%");
    video.setAttribute("height", "auto");
    video.setAttribute("controls", "");

    for (const child of theatre_ia.children)
	child.remove();

    theatre_ia.appendChild(video);
}
