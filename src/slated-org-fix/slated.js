/**
 * SPDX-License-Identifier: CC0-1.0
 *
 * On slated.org, automatically dismiss the warning saying you need
 * JavaScript and unhide the document body.
 *
 * Copyright (C) 2022 Wojtek Kosior <koszko@koszko.org>
 *
 * Available under the terms of Creative Commons Zero v1.0 Universal.
 */

/* Use with: https://slated.org/*** */

for (const bad_elem_id of [
    "seckit-noscript-tag",
    "seckit-clickjacking-no-body"
]) {
    const bad_elem = document.getElementById(bad_elem_id);
    if (bad_elem !== null)
	bad_elem.remove();
}
