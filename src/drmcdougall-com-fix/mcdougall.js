/**
 * SPDX-License-Identifier: CC0-1.0
 *
 * View OpenCores projects list without nonfree js
 *
 * Copyright (C) 2021 Wojtek Kosior <koszko@koszko.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the CC0 1.0 Universal License as published by
 * the Creative Commons Corporation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * CC0 1.0 Universal License for more details.
 */

/* Use with https://www.drmcdougall.com/*** */

const banner_class = "floating-action-button-container";
for (const banner of document.getElementsByClassName(banner_class))
    banner.remove()

for (const element of document.body.querySelectorAll("noscript")) {
    const replacement = document.createElement('haketilo-noscript');

    /*
     * Make sure only images get added. For maximum security make sure they
     * are using https:// scheme instead of dangerous data:.
     */
    for (const subelem of [...replacement.querySelectorAll("*")]) {
        if (subelem.tagName !== "IMG")
            subelem.remove();

        const best_src = subelem.getAttribute('data-src');
        if (best_src.startsWith("https://"))
            subelem.src = best_src;
        else
            subelem.remove();
    }

    element.replaceWith(replacement);
}
