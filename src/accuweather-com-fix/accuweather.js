/**
 * SPDX-License-Identifier: CC0-1.0
 *
 * View Accuweather forecasts without nonfree js.
 *
 * Copyright (C) 2022 Wojtek Kosior <koszko@koszko.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the CC0 1.0 Universal License as published by
 * the Creative Commons Corporation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * CC0 1.0 Universal License for more details.
 */

for (const node of [...document.getElementsByClassName("non-ad")])
    node.classList.remove("non-ad");
