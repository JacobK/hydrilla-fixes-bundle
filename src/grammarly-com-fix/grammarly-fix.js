/**
 * SPDX-License-Identifier: LicenseRef-GPL-3.0-or-later-WITH-js-exceptions
 *
 * Allow dismissing the prompt to download Grammarly and display images.
 *
 * Copyright (C) 2022 Jacob K
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute forms of that code without the copy of the GNU
 * GPL normally required by section 4, provided you include this
 * license notice and, in case of non-source distribution, a URL
 * through which recipients can access the Corresponding Source.
 * If you modify file(s) with this exception, you may extend this
 * exception to your version of the file(s), but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * As a special exception to the GPL, any HTML file which merely
 * makes function calls to this code, and for that purpose
 * includes it by reference shall be deemed a separate work for
 * copyright law purposes. If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so. If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Use with:https://www.grammarly.com/*** */

/* add event listener to class "_124ZW-close" to delete class "_1X8pZ-overlay"
   in order to make it possible to decline to install Grammarly (You cannot
   click "Get Grammarly", but why would you want to do that?) */
if (document.getElementsByClassName("_124ZW-close")[0]) { // The popup doesn't always appear
    document.getElementsByClassName("_124ZW-close")[0]
	.addEventListener("click", function() {
	    document.getElementsByClassName("_1X8pZ-overlay")[0].remove();
	});
} /* The class names seem like something that could change often, but they
     haven't changed in over a month (2022-04-15 to 2022-05-21). */

// make it so images display (except for tracking pixels)
[...document.getElementsByTagName("img")].forEach(function(element){
    const data_src = element.getAttribute("data-src");

    if (data_src === null || element.hasAttribute("src"))
	return;

    try {
	if (new URL(data_src).host.endsWith("pixel.newscred.com")) {
	    element.remove();
	    return;
	}
    } catch(e) {
	console.error(e);
    }

    element.src = data_src;
});
