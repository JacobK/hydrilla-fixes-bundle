/**
 * SPDX-License-Identifier: Apache-2.0
 *
 * (Incomplete) Fix for Google Forms
 *
 * Copyright © 2021 jahoti <jahoti@tilde.team>
 * Copyright 2022 Wojtek Kosior <koszko@koszko.org>
 * Copytight 2022 Jacob K
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * I, Wojtek Kosior, thereby promise not to sue for violation of this file's
 * license. Although I request that you do not make use of this code in a way
 * incompliant with the license, I am not going to enforce this in court.
 */

var form = document.forms[0];

/* Fix form fields. */
for (let div of form.querySelectorAll('div[data-params], div.geS5n.AgroKb.oQYVNd')) {
    /*
     * The selector, "div.geS5n.AgroKb.oQYVNd", is to catch email fields, which
     * don't have a data-params attribute.
     */
    if (div.dataset.params) {
	const data = JSON.parse('[' + div.dataset.params.substring(4));
	var name = 'entry.' + data[0][4][0][0];
    } else if (div.className == "geS5n AgroKb oQYVNd") {
	var name = "emailAddress";
    } else {
	console.error(`Cannot enable input`, div);
	continue;
    }
    var input = div.querySelector('input, textarea');

    if (!input) {
	console.error(`cannot enable input ${name}`, div);
	continue;
    }

    if (input.name === name + '_sentinel') {
	/* Handle radio buttons. */
	for (const input_div of div.querySelectorAll('[data-value]')) {
	    const new_radio = document.createElement('input');
	    new_radio.type = 'radio';
	    new_radio.name = name;
	    new_radio.value = input_div.getAttribute("data-value");
	    input_div.replaceWith(new_radio);
	}
	/* Handle checkboxes. */
	for (const input_div of div.querySelectorAll('[data-answer-value]')) {
	    const new_checkbox = document.createElement('input');
	    new_checkbox.type = 'checkbox';
	    new_checkbox.name = name;
	    new_checkbox.value = input_div.getAttribute("data-answer-value");
	    input_div.replaceWith(new_checkbox);
	}
    } else {
	input.removeAttribute('disabled');
	input.name = name;

	/* Enlarge textareas and make them stand out from mere input fields. */
	if (input.tagName === "TEXTAREA") {
	    input.style.height = "8em";
	    input.style.overflowY = "scroll";
	}
    }
}

/* Remove placeholders in text input fields and textareas. */
document.querySelectorAll('[jsname=LwH6nd]').forEach(n => n.remove());

/* Enable the form sumbission button (if any). */
for (const submit_but of document.querySelectorAll('[jsname=M2UYVd]'))
    submit_but.addEventListener("click", () => {
	/*
	 * hacktcha_completed() will be defined by google-forms-hacktcha.js but only
	 * if we have the actual captcha support enabled. It will return true if
	 * we're already authenticated.
	 */
	if (typeof window.hacktcha_completed === "function") {
	    if (!window.hacktcha_completed())
		return;
	}

	form.submit()
    });

/* Enable the "next page" button (if any). */
function goToNext()
{
    var next = document.createElement('input');
    next.type = 'hidden';
    next.name = 'continue';
    next.value = '1';
    form.appendChild(next);
    form.submit();
}

for (const next_but of document.querySelectorAll('[jsname=OCpkoe]'))
    next_but.addEventListener("click", goToNext);

/* Remove no JavaScript warning (if any). */
for (const warning of
     document.querySelectorAll(".HB1eCd-X3SwIb-i8xkGf, noscript"))
    warning.remove();

/* TODO:
 * support "back" with instatiation of previous entries
 * find and fix form parts that still don't work (if any)
   * support dropdown inputs
   * support "other" radio buttons and "other" checkbox buttons, in which the
     user types text in addition to selecting the "other" box
 * support displaying public analytics ("viewanalytics" instead of "viewform")
*/
