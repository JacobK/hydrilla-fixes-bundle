/**
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright © 2021 jahoti <jahoti@tilde.team>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

function submitFormItem() {
	var name, val, queryString = '', xhr = new content.XMLHttpRequest();
	for (var formItem of this.querySelectorAll('select, input:not([type="radio"]):not([type="checkbox"])' +
		':not([type="submit"]):not([type="reset"])')) {
		queryString += (queryString && '&') + formItem.name + '=' + encodeURIComponent(formItem.value);
	}

	xhr.onreadystatechange = function () {
		if (this.readyState === 4) {
			if (this.status === 200) location.href =  JSON.parse(this.responseText).follow_up_url;
			else if (this.status === 422) {
				var failMessage = [], response = JSON.parse(this.responseText);
				for (field in response.errors) for (error of response.errors[field]) {
					failMessage.push('Field "' + field + '" ' + error);
				}
				alert(failMessage.join('\n'));
			}
			else alert('Submission failed: response code ' + this.status);
		}
	}

	xhr.open('POST', this.action, true); // Manually add the domain, as it's not properly handled in extensions
	xhr.setRequestHeader('X-CSRF-Token', csrf);
	xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.send(queryString);
	return false;
}

// Apply CSS as necessary
if (notice = document.querySelector('#petition-bar-main > span')) notice.style.display = 'none'; // Hide the totally mistaken (even without this extension) anti-anti-JS warning
document.querySelector('.script-dependent').style.display = 'block';
document.querySelector('.button-wrapper').style.position = 'static'; // Stop the "submit" button obscuring the form



csrf = document.querySelector('meta[name="csrf-token"]').content
for (var button of document.querySelectorAll('button[type="submit"].button.action-form__submit-button')) button.form.onsubmit = submitFormItem;
