/**
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright © 2021 jahoti <jahoti@tilde.team>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var div, player, playerBox = document.querySelector('.inline_player');
playerBox.innerHTML = '';

for (var track of JSON.parse(document.querySelector('[data-tralbum]').dataset.tralbum).trackinfo) {
	div = document.createElement('div');
	player = document.createElement('audio');
	player.controls = 'controls';

	div.innerText = track.title + ': ';
	player.src = track.file['mp3-128']; // Is this always available?
	div.append(player);
	playerBox.append(div);
}
